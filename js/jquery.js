'use strict'; 

let controller = new ScrollMagic.Controller();

let scene = new ScrollMagic.Scene({

    triggerElement: '.box'


})

    .setClassToggle('.box', 'fade-in')
    .addIndicators({
        name: 'INDICATIONS',
        indent: 200,
        colorStart: '#fff'
    })
    .addTo(controller);